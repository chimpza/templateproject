
### Template project to use when creating a new ###

* This is a base template app for all my projects.  It illustrates my preferred directory structure and the technology stack that  I feel is currently best of breed.  There are simple examples of each included.
* It is currently using Dagger2 for dependency injection and ReactiveX (enable with RXJava, RXAndroid, etc.) for asynchronous and event-based programming.  
* It is kept up-to-date with any new stable library changes. 
* Version 1.0


### Who do I talk to? ###

* For any questions/suggestions mail me at adam@writtenbyaliens.com