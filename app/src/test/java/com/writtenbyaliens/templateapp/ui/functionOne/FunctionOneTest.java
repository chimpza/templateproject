package com.writtenbyaliens.templateapp.ui.functionOne;

// Static imports for assertion methods

import android.os.Build;
import android.support.v7.widget.RecyclerView;

import com.writtenbyaliens.templateapp.BuildConfig;
import com.writtenbyaliens.templateapp.R;
import com.writtenbyaliens.templateapp.data.api.MockApiService;
import com.writtenbyaliens.templateapp.data.models.Crime;
import com.writtenbyaliens.templateapp.utils.DataUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


/**
 * Roboelectric for simulated tests
 *
 * Created by Adz on 08/03/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class FunctionOneTest {

    FunctionOneActivity mActivity;

    @Before
    public void setUp() throws Exception {
        mActivity = Robolectric.setupActivity(FunctionOneActivity.class);
    }


    /**
     * Ensure that the recyclerview is showing the correct number of items.
     * Uses hand-coded mock api service from the debug build
     */
    @Test
    public void checkCorrectItemCount() {
        RecyclerView mCrimeList = (RecyclerView) mActivity.findViewById(R.id.crime_list);
        assertThat(mCrimeList.getChildCount() == 2);
    }

    /**
     * An simple example of a test using Mockito
     */
    @Test
    public void checkDataUtils() {
        //  create mock
        DataUtils test = Mockito.mock(DataUtils.class);

        // define return value for method getUniqueId()
        when(test.roundNumber(42.2)).thenReturn((long)43);

        // use mock in test....
        assertEquals(test.roundNumber(42.2), (long)43);

    }

    /**
     * Check number of items returned from service method
     */
    @Test
    public void checkDataItemsLength() {
        //  Create mock of API service.
        MockApiService test = Mockito.mock(MockApiService.class);

        // Define return value for method getUniqueId()
        Crime crime = Crime.newCrime()
                .id(101)
                .category("Naughty")
                .monthOccurred("2012-2")
                .build();

        Crime crime2 = Crime.newCrime()
                .id(102)
                .category("Very Naughty")
                .monthOccurred("2012-3")
                .build();
        List<Crime> crimes = new ArrayList<>();
        crimes.add(crime);
        crimes.add(crime2);

        when(test.getCrimes(52.3,21.2)).thenReturn(Observable.from(crimes).toList());

        // Use mock in test
        test.getCrimes(52.3, 21.2)
                .subscribeOn(Schedulers.io())
                .subscribe(results -> assertEquals(results.size(), 2), throwable -> Timber.e(throwable,"Error testing"));
    }


}
