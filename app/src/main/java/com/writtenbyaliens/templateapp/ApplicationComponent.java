package com.writtenbyaliens.templateapp;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application.
 * <p>
 * Created by Adz on 04/03/2016.
 */

@Singleton // Constraints this component to one-per-application.
@Component(modules = TemplateApplicationModule.class)
public interface ApplicationComponent {

    void inject(TemplateApplication application);

    // Exported for child-components.
    Application application();
}
