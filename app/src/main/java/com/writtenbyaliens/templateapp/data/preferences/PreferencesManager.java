package com.writtenbyaliens.templateapp.data.preferences;

/**
 * Created by Adz on 04/03/2016.
 */
public class PreferencesManager {
    private static PreferencesManager ourInstance = new PreferencesManager();

    public static PreferencesManager getInstance() {
        return ourInstance;
    }

    private PreferencesManager() {
    }
}
