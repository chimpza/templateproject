package com.writtenbyaliens.templateapp.data.api;

import com.writtenbyaliens.templateapp.data.models.Crime;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Service used to connect to an external API
 *
 * Created by Adz on 04/03/2016.
 */
public interface ApiService {

    @GET("all-crime")
    Observable<List<Crime>> getCrimes(@Query("lat") Double lat, @Query("lng") Double lng);

}
