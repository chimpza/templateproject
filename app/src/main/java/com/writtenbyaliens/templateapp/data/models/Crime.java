package com.writtenbyaliens.templateapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adz on 01/09/2015.
 */
public class Crime implements Parcelable {

    @SerializedName("id")
    private long id;

    @SerializedName("category")
    private String category;

    @SerializedName("location")
    private Location location;

    @SerializedName("month")
    private String monthOccurred;

    private Crime(Builder builder) {
        id = builder.id;
        category = builder.category;
        location = builder.location;
        monthOccurred = builder.monthOccurred;
    }

    public static Builder newCrime() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public Location getLocation() {
        return location;
    }

    public String getMonthOccurred() {
        return monthOccurred;
    }


    public static final class Builder {
        private long id;
        private String category;
        private Location location;
        private String monthOccurred;

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder category(String val) {
            category = val;
            return this;
        }

        public Builder location(Location val) {
            location = val;
            return this;
        }

        public Builder monthOccurred(String val) {
            monthOccurred = val;
            return this;
        }

        public Crime build() {
            return new Crime(this);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.category);
        dest.writeParcelable(this.location, flags);
        dest.writeString(this.monthOccurred);
    }

    protected Crime(Parcel in) {
        this.id = in.readLong();
        this.category = in.readString();
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.monthOccurred = in.readString();
    }

    public static final Parcelable.Creator<Crime> CREATOR = new Parcelable.Creator<Crime>() {
        public Crime createFromParcel(Parcel source) {
            return new Crime(source);
        }

        public Crime[] newArray(int size) {
            return new Crime[size];
        }
    };
}
