package com.writtenbyaliens.templateapp;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by Adz on 04/03/2016.
 */
public class TemplateApplication extends Application {

    private ApplicationComponent applicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .templateApplicationModule(new TemplateApplicationModule(this))
                .build();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            // TODO Add release debugging service here, e.g. Crashlytics.start(this);
            // TODO Timber.plant(new CrashlyticsTree());
        }

    }

    public ApplicationComponent component() {
        return applicationComponent;
    }

}
