package com.writtenbyaliens.templateapp;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * * A module for Android-specific dependencies which require a context or
 * {@link android.app.Application} to create.
 * <p>
 * Created by Adz on 04/03/2016.
 */
@Module
public class TemplateApplicationModule {
    private final Application application;

    public TemplateApplicationModule(Application application) {
        this.application = application;
    }

    /**
     * Expose the application to the graph.
     */
    @Provides
    @Singleton
    Application application() {
        return application;
    }

    // TODO Add any application scope objects here. E.g. Location, bluetooth services:
    /*@Provides @Singleton LocationManager provideLocationManager() {
        return (LocationManager) application.getSystemService(LOCATION_SERVICE);
    }*/
}
