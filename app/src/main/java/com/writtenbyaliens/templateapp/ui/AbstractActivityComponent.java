package com.writtenbyaliens.templateapp.ui;

import android.app.Activity;

import com.writtenbyaliens.templateapp.ApplicationComponent;

import dagger.Component;

/**
 * A base component upon which fragment's components may depend.  Activity-level components
 * should extend this component.
 * Created by Adz on 04/03/2016.
 */

@PerActivity // Subtypes of AbstractActivityComponent should be decorated with @PerActivity.
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface  AbstractActivityComponent {
    Activity activity(); // Expose the activity to sub-graphs
}
