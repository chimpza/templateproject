package com.writtenbyaliens.templateapp.ui.functionOne;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.writtenbyaliens.templateapp.R;
import com.writtenbyaliens.templateapp.data.models.Crime;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Adz on 06/03/2016.
 */
public class CrimeRecylerviewAdapter extends RecyclerView.Adapter<CrimeRecylerviewAdapter.CrimeViewHolder> {

    //================================================================================
    // MEMBER VARIABLES
    //================================================================================

    private final List<Crime> mItems;
    private final OnCrimeClickListener mCrimeListener;

    //================================================================================
    // CONSTRUCTOR
    //================================================================================

    public CrimeRecylerviewAdapter(List<Crime> items, OnCrimeClickListener crimeListener) {
        this.mItems = items;
        this.mCrimeListener = crimeListener;
    }

    //================================================================================
    // RECYCLERVIEW METHODS
    //================================================================================

    @Override
    public CrimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item, parent, false);
        return new CrimeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CrimeViewHolder holder, int position) {
        holder.bind(mItems.get(position), mCrimeListener);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    //================================================================================
    // VIEW HOLDERS
    //================================================================================
    static class CrimeViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.list_item)
        TextView name;

        @Bind(R.id.list_date)
        TextView date;

        @Bind(R.id.list_id)
        TextView id;

        public CrimeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Crime crime, final OnCrimeClickListener listener) {
            id.setText(Long.toString(crime.getId()));
            name.setText(crime.getCategory());
            date.setText(crime.getMonthOccurred());
            itemView.setOnClickListener(click ->
                    listener.onItemClick(crime)
            );
        }

    }

    //================================================================================
    // LISTENERS
    //================================================================================

    public interface OnCrimeClickListener {
        void onItemClick(Crime item);
    }

}
