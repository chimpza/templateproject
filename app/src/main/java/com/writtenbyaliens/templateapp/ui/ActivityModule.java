package com.writtenbyaliens.templateapp.ui;

import android.app.Activity;
import android.app.Application;
import android.location.LocationManager;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A module to expose the activity to the graph
 * <p/>
 * Created by Adz on 04/03/2016.
 */
@Module
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity() {
        return activity;
    }

    // Example of  something we can provide to an activity.
    // NB This particular example would be better provided at application level though
    @Provides
    @PerActivity
    LocationManager provideLocationManager(Application application) {
        return (LocationManager) application.getSystemService(LOCATION_SERVICE);
    }

}
