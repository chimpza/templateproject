package com.writtenbyaliens.templateapp.ui.functionOne;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.writtenbyaliens.templateapp.R;
import com.writtenbyaliens.templateapp.TemplateApplication;
import com.writtenbyaliens.templateapp.data.api.ApiService;
import com.writtenbyaliens.templateapp.data.models.Crime;
import com.writtenbyaliens.templateapp.ui.ActivityModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class FunctionOneActivity extends RxAppCompatActivity {

    //================================================================================
    // MEMBER CONSTANTS
    //================================================================================

    private static final String ARG_CRIME_DATA = "crime_data";

    //================================================================================
    // MEMBER VARIABLES
    //================================================================================

    private FunctionOneComponent component;
    private List<Crime> mCrimeData;

    //================================================================================
    // INJECTED OBJECTS
    //================================================================================

    @Inject
    ApiService mApiManager;

    //================================================================================
    // VIEWS
    //================================================================================

    @Bind(R.id.crime_list)
    RecyclerView mCrimeList;

    @Bind(R.id.loading_progress)
    ProgressBar mProgress;

    //================================================================================
    // COMPONENT SET UP
    //================================================================================

    /**
     * Set up the component that will be used to inject the dependencies
     *
     * @return
     */
    FunctionOneComponent component() {
        if (component == null) {
            component = DaggerFunctionOneComponent.builder()
                    .applicationComponent(((TemplateApplication) getApplication()).component())
                    .activityModule(new ActivityModule(this))
                    .build();
        }
        return component;
    }

    //================================================================================
    // LIFECYCLE
    //================================================================================

    /**
     * Try to keep this method as tidy and un-monolithic as possible
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        component().inject(this);
        setContentView(R.layout.activity_function_one);
        ButterKnife.bind(this);
        initInstanceData(savedInstanceState);
        getData();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARG_CRIME_DATA, (ArrayList) mCrimeData);
    }

    //================================================================================
    // INITALISATION
    //================================================================================

    /**
     * Get instance data so we don't have to get it from the api if the activity gets destroyed
     *
     * @param savedInstanceState
     */
    private void initInstanceData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCrimeData = savedInstanceState.getParcelableArrayList(ARG_CRIME_DATA);
        }
    }

    //================================================================================
    // DATA METHODS
    //================================================================================

    /**
     * Gets the data from the api if it hasn't been downloaded yet
     */
    private void getData() {
        showProgress(true);
        if (mCrimeData == null || mCrimeData.isEmpty()) {
            mApiManager.getCrimes(51.4368212, -2.6090684)
                    .flatMap(Observable::from)
                    .toSortedList((crime, crime2) -> compareId(crime, crime2))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(crimes -> {
                        Timber.d( "Got data. Count:" + crimes.size());
                        mCrimeData = crimes;
                        showResults();
                    }, throwable -> showError(throwable));
        } else {
            showResults();
        }
    }

    private static Integer compareId(Crime crime1, Crime crime2) {
        return crime1.getId() > crime2.getId() ? 1 : 0;
    }

    //================================================================================
    // UI METHODS
    //================================================================================

    /**
     * Populates the list with crime data from the api
     */
    private void showResults() {
        CrimeRecylerviewAdapter crimeRecylerviewAdapter = new CrimeRecylerviewAdapter(mCrimeData, onCrimeClickListener);
        mCrimeList.setAdapter(crimeRecylerviewAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mCrimeList.setHasFixedSize(true);
        mCrimeList.setLayoutManager(mLayoutManager);
        showProgress(false);
    }

    private void showError(Throwable throwable) {
        Timber.e(throwable, "Error occurred getting data",null);
        Toast.makeText(this, "Error " + throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    private void showProgress(boolean show) {
        if (show) {
            mProgress.setVisibility(View.VISIBLE);
            mCrimeList.setVisibility(View.GONE);
        } else {
            mProgress.setVisibility(View.GONE);
            mCrimeList.setVisibility(View.VISIBLE);
        }
    }

    //================================================================================
    // EVENTS
    //================================================================================

    /**
     * Click handler for when a crime has been clicked on
     */
    CrimeRecylerviewAdapter.OnCrimeClickListener onCrimeClickListener = item -> {
        Toast.makeText(this, "Clicked on:  " + item.getId() + " " + item.getCategory(), Toast.LENGTH_LONG).show();
    };

}
