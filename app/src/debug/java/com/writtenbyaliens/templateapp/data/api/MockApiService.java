package com.writtenbyaliens.templateapp.data.api;

import com.writtenbyaliens.templateapp.data.models.Crime;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Adz on 07/03/2016.
 */
public class MockApiService implements ApiService {

    @Override
    public Observable<List<Crime>> getCrimes(@Query("lat") Double lat, @Query("lng") Double lng) {

        List<Crime> crimes = new ArrayList<>();

        Crime CRIME1 = Crime.newCrime()
                .id(100)
                .monthOccurred("2015-2")
                .category("Very naughty crime")
                .build();

        Crime CRIME2 = Crime.newCrime()
                .id(101)
                .monthOccurred("2015-2")
                .category("Very very naughty crime")
                .build();

        crimes.add(CRIME1);
        crimes.add(CRIME2);

        return Observable.just(crimes);
    }
}
