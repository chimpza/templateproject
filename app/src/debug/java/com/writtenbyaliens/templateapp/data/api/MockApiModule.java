package com.writtenbyaliens.templateapp.data.api;

import com.writtenbyaliens.templateapp.ui.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Adz on 04/03/2016.
 */
@Module
public class MockApiModule {

    @Provides
    @PerActivity
    ApiService apiService() {
        return new MockApiService();
    }


}
