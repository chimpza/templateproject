package com.writtenbyaliens.templateapp.data.api;

import com.writtenbyaliens.templateapp.ui.PerActivity;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Adz on 04/03/2016.
 */
@Module
public class ReleaseApiModule {

    public static final String PRODUCTION_API_URL = "https://data.police.uk/api/crimes-street/";

    @Provides
    @PerActivity
    ApiService apiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @PerActivity
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(PRODUCTION_API_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()) // For Rx
                .addConverterFactory(GsonConverterFactory.create()) //For GSON
                .build();
    }


    /**
     * Use an inteceptor to customise the request. E.g. to add Authorization and Accept headers that
     * the API may require.
     * <p>
     * We can also add some http loggin here
     *
     * @return
     */
    @Provides
    @PerActivity
    OkHttpClient provideHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        // Header interceptor
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            // Customize the request
            Request request = original.newBuilder()
                    .header("Accept", "application/json")
                    // .header("Authorization", "auth-token")
                    .method(original.method(), original.body())
                    .build();

            Response response = chain.proceed(request);

            // Customize or return the response
            return response;
        });

        // Logging interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        return httpClient.build();
    }


}
