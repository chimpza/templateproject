package com.writtenbyaliens.templateapp.ui.functionOne;

import com.writtenbyaliens.templateapp.ApplicationComponent;
import com.writtenbyaliens.templateapp.data.api.ReleaseApiModule;
import com.writtenbyaliens.templateapp.ui.AbstractActivityComponent;
import com.writtenbyaliens.templateapp.ui.ActivityModule;
import com.writtenbyaliens.templateapp.ui.PerActivity;

import dagger.Component;

/**
 * Created by Adz on 04/03/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ReleaseApiModule.class})
public interface FunctionOneComponent extends AbstractActivityComponent {
    void inject(FunctionOneActivity functionOneActivity);
}
